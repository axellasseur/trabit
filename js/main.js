const $ = require('jquery');

function currentTime(){
	let now = new Date();
	let month = now.getMonth()+1;
	let hours = now.getHours();
	let minutes = now.getMinutes();
	let seconds = now.getSeconds();
	if(month < 10) month = '0'+month;
	if(hours < 10) hours = '0'+hours;
	if(minutes < 10) minutes = '0'+minutes;
	if(seconds < 10) seconds = '0'+seconds;
	return now.getDate()+"/"+month+"/"+now.getFullYear()+" "+hours+":"+minutes+":"+seconds;
}

function setLog(input){
	let logs = (localStorage.logs) ? JSON.parse(localStorage.logs) : [];
	logs.push(currentTime() + " : "+input);
	localStorage.logs = JSON.stringify(logs);

	console.log(JSON.parse(localStorage.logs));
}

window.onload = function(){
	console.log("start");
	setLog("Start");

	$("table#circle-three-pairs").hide();
	var bitstamp = new Bitstamp(config.client_id, config.api_key, config.api_secret);
	//list all the channels needed
	var pusher = new Pusher('de504dc5763aeef9ff52');
	var channels = [], i = 0;
	var init = {
		fees: 0,
		wallet: 0
	};
	init.wallet = $('.initial_wallet').val();
	init.fees = $('.fees').val();

	circle.forEach(function(e){
		e.pair.forEach(function(elem) {
			if(elem.currency1 == 'btc' && elem.currency2 == 'usd')
				channels[elem.currency1+elem.currency2] = pusher.subscribe('order_book');
			else
				channels[elem.currency1+elem.currency2] = pusher.subscribe('order_book_'+elem.currency1+elem.currency2);
		});
	});

	setInterval(lastTickerTimer, 1000);
	function lastTickerTimer(){
		$('.circle-three-pairs').each(function(index){
			$(this).find('tr.firstpair').find('td.last-ticker').html(cooldownCalculation(circle[index].pair[0].lastTicker));
			$(this).find('tr.secondpair').find('td.last-ticker').html(cooldownCalculation(circle[index].pair[1].lastTicker));
			$(this).find('tr.thirdpair').find('td.last-ticker').html(cooldownCalculation(circle[index].pair[2].lastTicker));

			//beautification
			if($(this).find('tr.firstpair').find('td.last-ticker').html() != "waiting")
				$(this).find('tr.firstpair').removeClass('waiting');
			if($(this).find('tr.secondpair').find('td.last-ticker').html() != "waiting")
				$(this).find('tr.secondpair').removeClass('waiting');
			if($(this).find('tr.thirdpair').find('td.last-ticker').html() != "waiting")
				$(this).find('tr.thirdpair').removeClass('waiting');
		});
	}

	function cooldownCalculation(lastTicker){
		if(lastTicker == 0)
			return "waiting";
		else{
			let time = Math.ceil(Date.now()/1000) - lastTicker;
			if(time > 59)
				time = Math.floor(time/60) + 'm' + (time%60) + 's';
			else
				time += 's';
			return time;
		}
	}

	function amountAfterTransaction(amount, price, fee, operation){
		//the amount have to get 8 numbers maximum after "."
		if(operation == 'buy')
			return Math.trunc((amount-fee) / price * 100000000)/100000000;
		else if(operation == 'sell')
			return Math.trunc((amount-fee) * price * 100000000)/100000000;
	}

	circle.forEach(function(c){
		//console.log(channels);
		let tablePairs = $("table#circle-three-pairs").clone().removeAttr('id').addClass('circle-three-pairs').show();

		/* fill table description */ 

		//pairs
		tablePairs.find('tr.firstpair').find('td.pair').html(c.pair[0].currency1.toUpperCase()+'/'+c.pair[0].currency2.toLocaleUpperCase());
		tablePairs.find('tr.secondpair').find('td.pair').html(c.pair[1].currency1.toUpperCase()+'/'+c.pair[1].currency2.toLocaleUpperCase());
		tablePairs.find('tr.thirdpair').find('td.pair').html(c.pair[2].currency1.toUpperCase()+'/'+c.pair[2].currency2.toLocaleUpperCase());
		//operation (buy or sell ?)
		tablePairs.find('tr.firstpair').find('td.operation').html(c.pair[0].operation);
		tablePairs.find('tr.secondpair').find('td.operation').html(c.pair[1].operation);
		tablePairs.find('tr.thirdpair').find('td.operation').html(c.pair[2].operation);

		$(".pane").append(tablePairs);

		c.pair.forEach(function(e){
			channels[e.currency1+e.currency2].bind('data', function(data){
				//console.log(`new ticker on ${e.currency1.toUpperCase()}/${e.currency2.toUpperCase()} : ${data.price} ${e.currency1}`);
				if(e.operation == 'buy')
					e.price = data.asks[0][0];
				else if(e.operation == 'sell')
					e.price = data.bids[0][0];

				e.lastTicker = Math.ceil(Date.now() / 1000);

				refresh();
			});
		});

		var refresh = function(){
			//last ticker column
			tablePairs.find('tr.firstpair').find('td.last-ticker').data("timestamp", c.pair[0].lastTicker);
			tablePairs.find('tr.secondpair').find('td.last-ticker').data("timestamp", c.pair[1].lastTicker);
			tablePairs.find('tr.thirdpair').find('td.last-ticker').data("timestamp", c.pair[2].lastTicker);

			//prices column
			tablePairs.find('tr.firstpair').find('td.price').html(c.pair[0].price);			
			tablePairs.find('tr.secondpair').find('td.price').html(c.pair[1].price);			
			tablePairs.find('tr.thirdpair').find('td.price').html(c.pair[2].price);			

			//amount column

			//buy
			var fee = Math.ceil((init.fees*init.wallet/100)*Math.pow(10, fee_precision[c.pair[0].currency2]))/Math.pow(10, fee_precision[c.pair[0].currency2]);
			tablePairs.find('tr.firstpair').find('td.fee').html(fee);
			c.pair[0].amount = amountAfterTransaction(init.wallet, c.pair[0].price, fee, c.pair[0].operation);

			//buy
			fee = Math.ceil((init.fees*c.pair[0].amount/100)*Math.pow(10, fee_precision[c.pair[1].currency2]))/Math.pow(10, fee_precision[c.pair[1].currency2]);			
			tablePairs.find('tr.secondpair').find('td.fee').html(fee);			
			c.pair[1].amount = amountAfterTransaction(c.pair[0].amount, c.pair[1].price, fee, c.pair[1].operation);

			//sell
			fee = Math.ceil((init.fees*c.pair[1].amount/100)*Math.pow(10, fee_precision[c.pair[2].currency2]))/Math.pow(10, fee_precision[c.pair[2].currency2]);							
			tablePairs.find('tr.thirdpair').find('td.fee').html(fee);
			c.pair[2].amount = amountAfterTransaction(c.pair[1].amount, c.pair[2].price, fee, c.pair[2].operation);

			tablePairs.find('tr.firstpair').find('td.amount').html(c.pair[0].amount);
			tablePairs.find('tr.secondpair').find('td.amount').html(c.pair[1].amount);
			tablePairs.find('tr.thirdpair').find('td.amount').html(c.pair[2].amount);

			//margin cell
			let margin = c.pair[2].amount-init.wallet;
			let marginPercentage = Math.trunc(margin * 10000 / init.wallet) / 100;
			tablePairs.find('th.margin').html(margin + ' | ' + marginPercentage + '%');

			if(Number.isNaN(margin)){
				tablePairs.find('th.margin').html('unable to calculate yet');
				tablePairs.find('th.margin').css("color", "grey");					
			}
			else if(margin >= 0){
				document.getElementById("alleluia").play();
				tablePairs.find('th.margin').css("color", "green");

				setLog("Matching prices ! Margin: "+margin+" | "+marginPercentage+"%");
			}
			else 
				tablePairs.find('th.margin').css("color", "red");			
		}

		$(".initial_wallet").on('change', function(){
			init.wallet = $('.initial_wallet').val();		
			refresh();	
		});

		$(".fees").on('change', function(){
			init.fees = $('.fees').val();		
			refresh();	
		});

		tablePairs.find("button").on("click", function(){
			alert('This function has been commented in the code');
		// 	bitstamp.submitRequest(bitstamp.methods.balance, {},
		// 	function(response){

		// 		/* check balances */

		// 		let error = false;
		// 		let currency0 = 'response.data.' + c.pair[0].currency2 + '_available'; //buy
		// 		if(eval(currency0) < init.wallet){
		// 			tablePairs.find('tr.firstpair').find('td.log').html(`only ${eval(currency0)} ${c.pair[0].currency2} available`);
		// 			error = true;
		// 		}

		// 		let currency1 = 'response.data.' + c.pair[1].currency2 + '_available'; //buy
		// 		if(eval(currency1) < c.pair[0].amount){
		// 			tablePairs.find('tr.secondpair').find('td.log').html(`only ${eval(currency1)} ${c.pair[1].currency2} available`);
		// 			error = true;
		// 		}

		// 		let currency2 = 'response.data.' + c.pair[2].currency1 + '_available'; //sell
		// 		if(eval(currency2) < c.pair[1].amount){
		// 			tablePairs.find('tr.thirdpair').find('td.log').html(`only ${eval(currency2)} ${c.pair[2].currency1} available`);
		// 			error = true;
		// 		}

		// 		/* orders */

		// 		if(!error){
		// 			//first pair
		// 			bitstamp.submitRequest(bitstamp.methods.orderbuy, {
		// 				currency_pair: c.pair[0].currency1 + c.pair[0].currency2, 
		// 				amount: c.pair[0].amount, 
		// 				price: c.pair[0].price
		// 			}, 
		// 			function(response){
		// 				if(response.data.status == "error")
		// 					tablePairs.find('tr.firstpair').find('td.log').html(response.data.reason.__all__[0]);
		// 				else{
		// 					tablePairs.find('tr.firstpair').find('td.log').html(response.data.datetime + ' ' + 'price: ' + response.data.price + ' amount: ' + response.data.amount);
		// 				}
		// 			});

		// 			//second pair
		// 			bitstamp.submitRequest(bitstamp.methods.orderbuy, {
		// 				currency_pair: c.pair[1].currency1 + c.pair[1].currency2, 
		// 				amount: c.pair[1].amount, 
		// 				price: c.pair[1].price
		// 			}, 
		// 			function(response){
		// 				if(response.data.status == "error")
		// 					tablePairs.find('tr.secondpair').find('td.log').html(response.data.reason.__all__[0]);
		// 				else{
		// 					tablePairs.find('tr.secondpair').find('td.log').html(response.data.datetime + ' ' + 'price: ' + response.data.price + ' amount: ' + response.data.amount);
		// 				}
		// 			});

		// 			//third pair
		// 			bitstamp.submitRequest(bitstamp.methods.ordersell, {
		// 				currency_pair: c.pair[2].currency1 + c.pair[2].currency2, 
		// 				amount: c.pair[1].amount, 
		// 				price: c.pair[2].price
		// 			}, 
		// 			function(response){
		// 				if(response.data.status == "error")
		// 					tablePairs.find('tr.thirdpair').find('td.log').html(response.data.reason.__all__[0]);
		// 				else{
		// 					tablePairs.find('tr.thirdpair').find('td.log').html(response.data.datetime + ' ' + 'price: ' + response.data.price + ' amount: ' + response.data.amount);
		// 				}
		// 			});
		// 		}
		// 	});
		// 	console.log('final_wallet: ' + c.pair[2].amount);
		});
	});
}