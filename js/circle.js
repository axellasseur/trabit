/*
fee_precision tell how many digits the total fee calculation takes for each currency. It has to define exactly the market behavior.
For example, on BTC/USD, the fee is calculated in USD and bitstamp round it 2 digits after the point, althought on ???/BTC, the round takes place 5 digits
*/
fee_precision = {
	'usd': 2,
	'btc': 5
};



circle = [
	{
		pair: [
			//bon en fait c'est mieux de sortir le final wallet et de mettre c.pair[0].amout = init.wallet
			//faut juste shifter
			{ currency1: 'btc', currency2: 'usd', operation: 'buy', price: 0, amount: 0, lastTicker: 0 }, //amount = currency1
			{ currency1: 'xrp', currency2: 'btc', operation: 'buy', price: 0, amount: 0, lastTicker: 0 }, //amount = currency1
			{ currency1: 'xrp', currency2: 'usd', operation: 'sell', price: 0, amount: 0, lastTicker: 0 } //amount = currency2 final wallet
		]
	},
	{
		pair: [
			{ currency1: 'btc', currency2: 'usd', operation: 'buy', price: 0, lastTicker: 0 },
			{ currency1: 'ltc', currency2: 'btc', operation: 'buy', price: 0, lastTicker: 0 },
			{ currency1: 'ltc', currency2: 'usd', operation: 'sell', price: 0, lastTicker: 0 }
		],
		amount: {
			currency1: 0,
			currency2: 0,
			final_wallet: 0
		}
	},
	{
		pair: [
			{ currency1: 'btc', currency2: 'usd', operation: 'buy', price: 0, lastTicker: 0 },
			{ currency1: 'eth', currency2: 'btc', operation: 'buy', price: 0, lastTicker: 0 },
			{ currency1: 'eth', currency2: 'usd', operation: 'sell', price: 0, lastTicker: 0 }
		],
		amount: {
			currency1: 0,
			currency2: 0,
			final_wallet: 0
		}
	},
	{
		pair: [
			{ currency1: 'btc', currency2: 'usd', operation: 'buy', price: 0, lastTicker: 0 },
			{ currency1: 'bch', currency2: 'btc', operation: 'buy', price: 0, lastTicker: 0 },
			{ currency1: 'bch', currency2: 'usd', operation: 'sell', price: 0, lastTicker: 0 }
		],
		amount: {
			currency1: 0,
			currency2: 0,
			final_wallet: 0
		}
	}
];