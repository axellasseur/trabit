# app [![XO code style](https://img.shields.io/badge/code_style-XO-5ed9c7.svg)](https://github.com/sindresorhus/xo)

> Caution ! This program is not well coded, there is no prompt, no security, I'm not sure about my calculation. It's still on progress, don't play with it like a dumb because the orders will be executed for real. I'm not responsible for any loss for you by using this code.

## Dev - Ubuntu

```
$ sudo apt install nodejs-legacy npm
$ sudo npm install electron-packager -g
$ sudo npm install bower -g
$ npm install
$ bower install
$ ./init.sh
```
Edit `config.js` to add your bitstamp ID and key

### Run

```
$ npm start
```

### Build

```
$ npm run build
```

Builds the app for macOS, Linux, and Windows, using [electron-packager](https://github.com/electron-userland/electron-packager).


## License

MIT © [<%= name %>](<%= website %>)
